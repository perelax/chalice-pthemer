# Chalice and pThemer

## Description
Chalice is a content provider which facilitates communication between pThemer and the OS.
pThemer is a simple color picker application.

![Lineage Kali Screens](https://gitlab.com/perelax/src_imgs/-/raw/main/Lineage%20Kali%20OnePlus/pThemer.jpg)

Chalice and pThemer are bundled with the system and are specific to the ROM below.  

[OnePlus 9 Pro LineageOS 19.1 Nethunter Enabled](https://gitlab.com/perelax/oneplus-9-9pro-lineage-19.1-nethunter-enabled)

Note: They will not allow Monet bypassing on other ROM's.

## Authors
Perelax

## License
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

See <https://www.gnu.org/licenses/> for a copy of the GNU General Public License.